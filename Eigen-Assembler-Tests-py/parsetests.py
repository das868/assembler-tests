# Ziel: Abhängigkeitsbaum erstellen, mit label außen und indented stuff innen
def parse_unsafe(filename="probeparsen.txt"):
    f = open(filename, 'r')
    print(f.readlines())
    f.close()
    return []


def parse(filename="probeparsen.txt"):
    lines = []
    #print("Here...")
    try:
        #print("Here2...")
        f = open(filename, 'r')
        #print("Here3...")
        for line in f.readlines():
            if(line[-1] == '\n'):
                lines.append(line[:-1])
                #print("Here4...")
            else:
                lines.append(line)
                #print("Here5...")
            #print(line)
        print(lines)
        return lines
    except Exception:
        print("R/W ERROR!")
    finally:
        #print("Here6...")
        f.close()


def getdepdict(filename):
    abhdict = dict()
    #while(True):
    counter = 0
    #print("Hello Parsetests!")
    #eingabe = input("Eingabe machen: ")
    #x = eingabe.split()
    #print(x)
    y = parse(filename)
    #print(y)
    z = []
    w = []
    for x in y:
        #print(x)
        a = x.split(';')
        #print("parsed: ", a)
        if(a == ['']): continue
        if(a[0][0:4] == '    ' or a[0][0] == '\t'):
            #print("--->INDENTED!")
            z.append(a[0])
            w.append(("INDENTED", a[0][4:]))
        elif(':' in a[0]):
            print("POSSIBLE LABEL: ", a[0])
            z.append(a[0][0:-1])
            w.append(("LABEL", a[0][0:-1])) #ohne Kolon
        else:
            z.append(a[0])
            w.append(("NORMAL", a[0]))
        
    #print("Z: \n", z)
    print("W: \n", w)

    #check if first item of w is indented, if so, throw error
    if(w[0][0] == "INDENTED"):
        print("Error: First statement indented, aborting...")
    else:
        print("\n---------------------\n")
        for k in range(0,len(w)):
            print("w[k]: ", w[k])
            if(w[k][0] == "NORMAL"):
                print("Normal item found: ", w[k][1])
                abhdict[counter] = w[k][1]
            elif(w[k][0] == "LABEL"):
                print("Label found: ", w[k][1])
                #abhdict[w[k][1][0:-1]] = [w[k][1][0:-1]] #ohne Kolon
                abhdict[w[k][1]] = []
            elif(w[k][0] == "INDENTED"):
                print("FOUND INDENT, FETCHING ROOT FOR ", w[k][1])
                i = 1
                while (w[k-i][0] != "LABEL"):
                    i = i+1
                print("found root: ", w[k-i][1])
                abhdict[w[k-i][1]].append(w[k][1])
            counter = counter+1
    print("\n\n------------------------\n", "abhdict: ", abhdict)
    return abhdict            
                    

        #break #DEBUG!
        #if(eingabe=="q"): break


def main():
    print("Hello Parsetests!")
    xyz = getdepdict("probeparsen.txt")
    print("\n----->Endergebnis: ", xyz)


if(__name__=="__main__"):
    main()