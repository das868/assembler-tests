'''
Keywords:
REGISTER A
REGISTER B
REGISTER C
REGISTER D
PROGCOUN PC
ZEROFLAG ZF
CARRFLAG CF
FLOWFLAG FF

MOV REG x
ADD REG y
SUB REG y
CMP x y
JMP line
INC REG
DEC REG
'''

# Initialisierung
a=b=c=d=pc= 0
zf=cf=ff = False
regstr = "A/`§_B/`§_C/`§_D/`§_"
regdict = {"A" : a, "B" : b, "C" : c, "D" : d}
instrdict = {"ADD" : (lambda a,b: a+b), "SUB" : (lambda a,b: a-b), "INC" : (lambda a: a+1), "DEC" : (lambda a: a-1)}
flagdict = {"ZF" : zf, "CF" : cf, "FF" : ff}


def parse(filename="probeanweisungen.txt"):
    try:
        f = open(filename, 'r')
        lines = []
        for line in f.readlines():
            if(line[-1] == '\n'):
                lines.append(line[:-1])
            else:
                lines.append(line)
            print(line)
        print(lines)
        return lines
    except Exception:
        print("R/W ERROR!")
    finally:
        f.close()


def getdepdict(filename):
    abhdict = dict()
    #while(True):
    counter = 0
    #print("Hello Parsetests!")
    #eingabe = input("Eingabe machen: ")
    #x = eingabe.split()
    #print(x)
    y = parse(filename)
    #print(y)
    z = []
    w = []
    for x in y:
        #print(x)
        a = x.split(';')
        #print("parsed: ", a)
        if(a == ['']): continue
        if(a[0][0:4] == '    ' or a[0][0] == '\t'):
            #print("--->INDENTED!")
            z.append(a[0])
            w.append(("INDENTED", a[0][4:]))
        elif(':' in a[0]):
            print("POSSIBLE LABEL: ", a[0])
            z.append(a[0][0:-1])
            w.append(("LABEL", a[0][0:-1])) #ohne Kolon
        else:
            z.append(a[0])
            w.append(("NORMAL", a[0]))
        
    #print("Z: \n", z)
    print("W: \n", w)

    #check if first item of w is indented, if so, throw error
    if(w[0][0] == "INDENTED"):
        print("Error: First statement indented, aborting...")
    else:
        print("\n---------------------\n")
        for k in range(0,len(w)):
            print("w[k]: ", w[k])
            if(w[k][0] == "NORMAL"):
                print("Normal item found: ", w[k][1])
                abhdict[counter] = w[k][1]
            elif(w[k][0] == "LABEL"):
                print("Label found: ", w[k][1])
                #abhdict[w[k][1][0:-1]] = [w[k][1][0:-1]] #ohne Kolon
                abhdict[w[k][1]] = []
            elif(w[k][0] == "INDENTED"):
                print("FOUND INDENT, FETCHING ROOT FOR ", w[k][1])
                i = 1
                while (w[k-i][0] != "LABEL"):
                    i = i+1
                print("found root: ", w[k-i][1])
                abhdict[w[k-i][1]].append(w[k][1])
            counter = counter+1
    print("\n\n------------------------\n", "abhdict: ", abhdict)
    return abhdict


def execute_mono(INSTR, OP1):
    if(INSTR in "INC/`§_DEC/`§_"):
        if(OP1 in regstr):
            regdict[OP1] = instrdict[INSTR](int(regdict[OP1]))
        else: print("Error in 1st arg of INC/DEC, skipping...")
    elif(INSTR == "JMP"):
        pass #TODO: add JMP functionality with labels
    else:
        print("ERROR! execute_mono: Unrecognizable command, skipping...")


def execute_duo(INSTR, OP1, OP2):
    if(INSTR == "MOV"):
        if(OP1 in regstr):    # funky separators no one will guess...
            if(OP2 in regstr):
                regdict[OP1] = int(regdict[OP2])
                #print("OP1: ", OP1, " , OP2: ", OP2, " , regdict[~]: ", regdict[OP1], " , regdict: ", regdict)
            elif(OP2.isdigit() or (OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-"))): # WRONG: elif(OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-")):
                regdict[OP1] = int(OP2)
            else: print("Error in 2nd arg of MOV, skipping...")
        else: print("Error in 1st arg of MOV, skipping...")
    elif(INSTR in "ADD/`§_SUB/`§_"):
        if(OP1 in regstr):
            if(OP2 in regstr):
                regdict[OP1] = instrdict[INSTR](int(regdict[OP1]), int(regdict[OP2]))
            elif(OP2.isdigit() or (OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-"))):
                regdict[OP1] = instrdict[INSTR](int(regdict[OP1]), int(OP2))
            else: print("Error in 2nd arg of ADD, skipping...")
        else: print("Error in 1st arg of ADD, skipping...")
    elif(INSTR == "CMP"):
        if(OP1 in regstr):
            if(OP2 in regstr):
                x = int(regdict[OP1])
                y = int(regdict[OP2])
                if(x==y):
                    flagdict["ZF"] = True
                    flagdict["CF"] = False
                elif(x>y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = False
                elif(x<y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = True
                else:
                    print("Error in REG-REG CMP comparison, skipping...")
            elif(OP2.isdigit() or (OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-"))):
                x = int(regdict[OP1])
                y = OP2
                if(x==y):
                    flagdict["ZF"] = True
                    flagdict["CF"] = False
                elif(x>y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = False
                elif(x<y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = True
                else:
                    print("Error in REG-NUM CMP comparison, skipping...")
            else:
                print("ERROR in 2nd arg of CMP, skipping...")
        elif(OP1.isdigit() or (OP1[1:].isdigit() and (OP1[0].isdigit() or OP1[0]=="-"))):
            if(OP2 in regstr):
                x = OP1
                y = int(regdict[OP2])
                if(x==y):
                    flagdict["ZF"] = True
                    flagdict["CF"] = False
                elif(x>y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = False
                elif(x<y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = True
                else:
                    print("Error in NUM-REG CMP comparison, skipping...")
            elif(OP2.isdigit() or (OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-"))):
                x = OP1
                y = OP2
                if(x==y):
                    flagdict["ZF"] = True
                    flagdict["CF"] = False
                elif(x>y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = False
                elif(x<y):
                    flagdict["ZF"] = False
                    flagdict["CF"] = True
                else:
                    print("Error in NUM-NUM CMP comparison, skipping...")
            else:
                print("Error, could not parse OP2 in CMP, skipping...")
        else:
            print("Error in 1st arg of CMP, skipping...")
    elif(INSTR in "INC/`§_DEC/`§_"):
        if(OP1 in regstr):
            regdict[OP1] = instrdict[INSTR](int(regdict[OP1]))
        else: print("Error in 1st arg of INC/DEC, skipping...")
    else:
        print("ERROR! execute_duo: Unrecognizable command, skipping...")


def jump(depdict, OP1):
    pass



def eventloop_input(): #No support for JMP/labels!
    while(True):
        x = input("Enter command ('quit' to exit):\t")
        if(x[0:4] == "quit"): break
        if(x[0:4] == "show"): print(regdict);continue
        if(x[0:4] == "flag"): print(flagdict);continue #print("zf: ", flagdict["ZF"], " ; cf: ", flagdict["CF"])
        stoppos = x.find(';')
        if(stoppos >= 0):
            x = x[0:x.find(';')]
        elif(stoppos < 0):
            #nix
            pass
        else:
            print("ERROR in ;-parser at beginning, doing nothing...")
        y = x.split()
        try:
            if(len(y) == 2):
                command = y[0]
                operand1 = y[1]
                print(" command = %s\n operand1 = %s" % (command, operand1))
                execute_mono(command, operand1)
            elif(len(y) == 3):
                command = y[0]
                operand1 = y[1]
                operand2 = y[2]
                print(" command = %s\n operand1 = %s\n operand2 = %s" % (command, operand1, operand2))
                execute_duo(command, operand1, operand2)
            else:
                print("ERROR! No parse, throwing error...\n parsed: ", y)
                raise IndexError(y)
        except IndexError:
            print("Input Error: IndexError caught, skipping...")
        finally:
            x=command=operand1=operand2=y="" # reset all local variables


def eventloop_fileread(): #No support for JMP/labels!
    content = parse()
    for x in content:
        if(x[0:4] == "quit"): break
        if(x[0:4] == "show"): print(regdict);continue
        if(x[0:4] == "flag"): print(flagdict);continue #print("zf: ", flagdict["ZF"], " ; cf: ", flagdict["CF"])
        stoppos = x.find(';')
        if(stoppos >= 0):
            x = x[0:x.find(';')]
        elif(stoppos < 0):
            #nix
            pass
        else:
            print("ERROR in ;-parser at beginning, doing nothing...")
        y = x.split()
        try:
            if(len(y) == 2):
                command = y[0]
                operand1 = y[1]
                print(" command = %s\n operand1 = %s" % (command, operand1))
                execute_mono(command, operand1)
            elif(len(y) == 3):
                command = y[0]
                operand1 = y[1]
                operand2 = y[2]
                print(" command = %s\n operand1 = %s\n operand2 = %s" % (command, operand1, operand2))
                execute_duo(command, operand1, operand2)
            else:
                print("ERROR! No parse, throwing error...\n parsed: ", y)
                raise IndexError(y)
        except IndexError:
            print("Input Error: IndexError caught, skipping...")
        finally:
            x=command=operand1=operand2=y="" # reset all local variables
    print(regdict)


def eventloop_file_dep(): #With dependency dict!
    depdict = getdepdict("probeparsen.txt") #probeanweisungen.txt , probeparsen.txt
    print("\n\n----->Entering main loop...")
    for key in depdict:
        print("key: ", key, "  ;  value: ", depdict[key])
        x = depdict[key]
        if(x[0:4] == "quit"): break
        if(x[0:4] == "show"): print(regdict);continue
        if(x[0:4] == "flag"): print(flagdict);continue #print("zf: ", flagdict["ZF"], " ; cf: ", flagdict["CF"])
        stoppos = x.index(';') # x.find(';')
        if(stoppos >= 0):
            x = x[0:x.find(';')]
        elif(stoppos < 0):
            #nix
            pass
        else:
            print("ERROR in ;-parser at beginning, doing nothing...")
        y = x.split()
        try:
            if(len(y) == 1):
                command = y[0]
                print(" command = ", command)
                if(command == "HLT"):
                    print("\n\n---------------->HALT REACHED, STOPPING!<------------------\n\n")
                    break
            elif(len(y) == 2):
                command = y[0]
                operand1 = y[1]
                print(" command = %s\n operand1 = %s" % (command, operand1))
                if(command == "JMP"):
                    jump(depdict, operand1)
                else:
                    execute_mono(command, operand1)
            elif(len(y) == 3):
                command = y[0]
                operand1 = y[1]
                operand2 = y[2]
                print(" command = %s\n operand1 = %s\n operand2 = %s" % (command, operand1, operand2))
                execute_duo(command, operand1, operand2)
            else:
                print("ERROR! No parse, throwing error...\n parsed: ", y)
                raise IndexError(y)
        except IndexError:
            print("Input Error: IndexError caught, skipping...")
        finally:
            x=command=operand1=operand2=y="" # reset all local variables
    #print(regdict)


def main():
    # Initialisierung
    '''a=b=c=d=pc= 0
    zf=False
    cf = False
    regstr = "A/`§_B/`§_C/`§_D/`§_"
    regdict = {"A" : a, "B" : b, "C" : c, "D" : d}
    instrdict = {"ADD" : (lambda a,b: a+b), "SUB" : (lambda a,b: a-b)}'''

    print("Hello")
    #getdepdict("probeanweisungen.txt")
    #eventloop_input()
    #eventloop_fileread()
    eventloop_file_dep()


if(__name__ == "__main__"):
    main()