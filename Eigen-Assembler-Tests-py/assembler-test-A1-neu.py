# Idee: Simulator bauen

class Memory:
    def __init__(self, name: str, length: int = 1, wordlength: int = 1, content: list = None):
        if content:
            self.name = name
            self.length = length
            self.wordlength = wordlength
            self.content = content
        else:
            self.name = name
            self.length = 1
            self.wordlength = 1
            self.content = [0b0]
    def show_content(self):
        resstr = "\nContents of Memory " + self.name + " :\n"
        for i in range(1,self.length+1):
            #print("LOS: i=", str(i), "\t i mod 16=", (i%16))
            if(i % 8 == 0):
                #print(bin(self.content[i]), sep=None, end='\n')
                resstr += str(bin(self.content[i-1])) + '\n'
            else:
                #print(bin(self.content[i]), sep=' ', end=None)
                resstr += str(bin(self.content[i-1])) + ' '
            print(resstr, sep=None)

    

ram = Memory("ram", 128, 16)
ram1 = Memory("ram1", 128, 16, [0b0011001100110011] * 128)
#print("Ram: ")
ram.show_content()
#print("\nRam1: ")
ram1.show_content()