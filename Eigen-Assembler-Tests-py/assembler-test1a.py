'''
Keywords:
REGISTER A
REGISTER B
REGISTER C
REGISTER D
PROGCOUN PC
ZEROFLAG ZF
CARRFLAG CF

MOV REG x
ADD x y
SUB x y
'''

# Initialisierung
a=b=c=d=pc= 0
zf=cf = False
regdict = {"A" : a, "B" : b, "C" : c, "D" : d}
instrdict = {"ADD" : (lambda a,b: a+b), "SUB" : (lambda a,b: a-b)}

def execute(INSTR, OP1, OP2):
    if(INSTR == "MOV"):
        if(OP1 in "A/`§_B/`§_C/`§_D/`§_"):    # funky separators no one will guess...
            if(OP2 in "A/`§_B/`§_C/`§_D/`§_"):
                regdict[OP1] = int(regdict[OP2])
                #print("OP1: ", OP1, " , OP2: ", OP2, " , regdict[~]: ", regdict[OP1], " , regdict: ", regdict)
            elif(OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-")):
                regdict[OP1] = int(OP2)
            else: print("Error in 2nd arg of MOV, skipping...")
        else: print("Error in 1st arg of MOV, skipping...")
    elif(INSTR in "ADD/`§_SUB/`§_"):
        if(OP1 in "A/`§_B/`§_C/`§_D/`§_"):
            if(OP2 in "A/`§_B/`§_C/`§_D/`§_"):
                regdict[OP1] = instrdict[INSTR](int(regdict[OP1]), int(regdict[OP2]))
            elif(OP2[1:].isdigit() and (OP2[0].isdigit() or OP2[0]=="-")):
                regdict[OP1] = instrdict[INSTR](int(regdict[OP1]), int(OP2))
            else: print("Error in 2nd arg of ADD, skipping...")
        else: print("Error in 1st arg of ADD, skipping...")
    else:
        print("ERROR! Unrecognizable command, skipping...")


def eventloop_input():
    while(True):
        x = input("Enter command ('quit' to exit):\t")
        if(x[0:4] == "quit"): break
        if(x[0:4] == "show"): print(regdict);continue
        y = x.split()
        try:
            command = y[0]
            operand1 = y[1]
            operand2 = y[2]
            print("command = %s\n operand1 = %s\n operand2 = %s" % (command, operand1, operand2))
            execute(command, operand1, operand2)
        except IndexError:
            print("Input Error: IndexError caught, skipping...")
        finally:
            x=command=operand1=operand2=y="" # reset all local variables


def eventloop_fileread():
    content = parse()
    for x in content:
        if(x[0:4] == "quit"): break
        if(x[0:4] == "show"): print(regdict);continue
        y = x.split()
        try:
            command = y[0]
            operand1 = y[1]
            operand2 = y[2]
            print("command = %s\n operand1 = %s\n operand2 = %s" % (command, operand1, operand2))
            execute(command, operand1, operand2)
        except IndexError:
            print("Input Error: IndexError caught, skipping...")
        finally:
            command=operand1=operand2=y="" # reset all local variables
    print(regdict)


def parse(filename="probeanweisungen.txt"):
    try:
        f = open(filename, 'r')
        lines = []
        for line in f.readlines():
            if(line[-1] == '\n'):
                lines.append(line[:-1])
            else:
                lines.append(line)
            print(line)
        print(lines)
        return lines
    except Exception:
        print("R/W ERROR!")
    finally:
        f.close()


print("Hello")
eventloop_input()
#eventloop_fileread()